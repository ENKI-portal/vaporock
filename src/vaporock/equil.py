from importlib import resources

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import thermoengine as thermo
from cycler import cycler
from IPython import display
from thermoengine import model

from . import chemistry as chem


class System:
    def __init__(self, liq_mod='v1.0', vapor_database='JANAF',
                 comp=None):
        self._init_liquid(liq_mod)
        self._init_vapor(vapor_database)
        self.R = self.vapor.R
        
        if comp is not None:
            self.set_melt_comp(comp)
            
    @property
    def vapor_elems(self):
        return list(self.vapor.species_comps.columns)
        
    @property
    def vapor(self):
        return self._vapor
    
    @property
    def melt_comp(self):
        return self._melt_comp
    
    @property
    def melt_elem_comp(self):
        return chem.convert_comp_to_elem_comp(self.melt_comp, elems=self.vapor_elems)
    
    @property
    def molar_oxide_comp(self):
        return dict(zip(self.OXIDES, self.oxide_mols))
    
    @property
    def mass(self):
        return self._mass
    
    @property
    def logfO2(self):
        return self._logfO2
    
    @property
    def T(self):
        return self._T
    
    @property
    def logP_species(self):
        return self._logP_species
    
    @property
    def P(self):
        P_species = 10**self.logP_species.values
        return np.sum(P_species)
        
    
    # Need to refactor, simplify storage/manipulation of chem options
    def set_melt_elem_comp(self, melt_elem_comp:pd.Series):
        oxide_mol_comp = chem.elem_to_oxide_mol_comp(melt_elem_comp)
        oxide_wt_comp = chem.oxide_mol_to_oxide_wt_comp(oxide_mol_comp)
        
        oxide_mol_comp_std_order = pd.Series(oxide_mol_comp.all_data).values
        mol_liq = chem.calc_liq_endmem_comp(oxide_mol_comp_std_order, self.liq_phs)
        mass = pd.Series(oxide_wt_comp.data).sum()
        
        self._melt_comp = oxide_wt_comp
        self.oxide_wts = oxide_wt_comp
        self.oxide_mols = oxide_mol_comp_std_order
        self.mol_liq = mol_liq
        self._mass = mass

    # Need to refactor, simplify storage/manipulation of chem options  
    def set_melt_comp(self, melt_comp_wts):
        oxide_wts = chem.format_oxide_wt_comp(melt_comp_wts)
        oxide_mols = chem.wt_to_mol_oxide(oxide_wts)
        mol_liq = chem.calc_liq_endmem_comp(oxide_mols, self.liq_phs)

        self._melt_comp = thermo.OxideWtComp(**melt_comp_wts)
        self.oxide_wts = oxide_wts
        self.oxide_mols = oxide_mols
        self.mol_liq = mol_liq
        self._mass = oxide_wts.sum()

    def print_melt_comp(self):
        liq_phs = self.liq_phs
        mol_liq = self.mol_liq

        print('MELTS liquid components:')
        display([(iendmem, imol) for iendmem, imol
                 in zip(liq_phs.endmember_names, mol_liq)])
    
    def _adjust_comp(self, elem_comp_input:dict):
        adjusted_elem_comp = self.melt_elem_comp+elem_comp_input
        self.set_melt_elem_comp(adjusted_elem_comp)
        return self
       
    def equilibrate(self, T, O2_buffer=None, intrinsic_O2=False, O2_method='equil'):
        P = 1

        if not intrinsic_O2:
            assert O2_buffer is not None, 'If not using intrinsic_O2, then O2_buffer must be defined (e.g. IW, NNO, QFM, etc.)'
            logfO2 = chem.redox_buffer(T, buffer=O2_buffer, P=P)
        
        if intrinsic_O2:
            
            if O2_method == 'equil':
                logfO2 = self._calc_logfO2_Kress91(T, self.oxide_mols)
            elif O2_method == 'stoic':
                logfO2_init = chem.redox_buffer(T, buffer='IW', P=P)
                logfO2 = logfO2_init
            else:
                assert False, f'Not Implemented: intrinsic_O2 procedure not defined for O2_method={O2_method}.'
        
        self._T = T
        self._O2_method = O2_method
        self._logfO2 = logfO2
        self._logP_species = self.eval_gas_abundances(T, logfO2)
        
        if intrinsic_O2 & (O2_method=='stoic'):
            self._calc_self_consistent_redox()
    
    @property    
    def vapor_elem_comp(self):
        mol_frac_species = 10**self.logP_species.iloc[:,0]/self.P
        mol_elems_vapor = mol_frac_species.dot(self.vapor.species_comps)
        return mol_elems_vapor/mol_elems_vapor.sum()
        
    def _calc_mol_elems_vapor_removed(self, frac_removed:float):
        mol_elems_vapor = self.vapor_elem_comp     
        vapor_mass_scale = chem.calc_elem_comp_mass(mol_elems_vapor)
        mass_removed = frac_removed*self.mass
        mol_elems_removed = mol_elems_vapor*(mass_removed/vapor_mass_scale)
        return mol_elems_removed
        
    def extract_vapor(self, T:float, O2_buffer=None, 
                      frac_removed:float=0.05) -> pd.Series:   
        
        if O2_buffer is None:
            intrinsic_O2 = True
        else:
            intrinsic_O2 = False
             
        self.equilibrate(T=T, O2_buffer=O2_buffer, intrinsic_O2=intrinsic_O2)
        mol_elems_removed =  self._calc_mol_elems_vapor_removed(frac_removed)
        self._adjust_comp(-mol_elems_removed)
        return mol_elems_removed
 
    def fractionate(self, T:float, O2_buffer=None, melt_frac_retained=0.1,
                    frac_removed=0.01):
        mass_init = self.mass
        
        while self.mass/mass_init > melt_frac_retained:
            self.extract_vapor(T, O2_buffer=O2_buffer, frac_removed=frac_removed)
        
        # self.extract_vapor(T, O2_buffer=O2_buffer, frac_removed=frac_removed)
        # self.extract_vapor(T, O2_buffer=O2_buffer, frac_removed=frac_removed)
 
 
    @property
    def log_oxygen_excess(self):
        T = self.T
        O2_method = self._O2_method
        if O2_method=='stoic':
            return self._calc_stoic_log_oxy_excess()
        elif O2_method=='equil':
            return self._calc_equil_log_oxy_excess(T)
        
        # return self._calc_stoic_log_oxy_excess()
        assert False, f'The O2_method given, {O2_method}, is not valid.'

    def eval_gas_abundances(self, T, logfO2, P=1e-10, full_output=False, 
                            method='activity'):

        vapor = self.vapor
        rxn_coefs = vapor.rxn_coefs
        mol_liq = self.mol_liq

        logP = []
        lnK = []
        lna = []

        T = np.array(T)
        logfO2 = np.array(logfO2)
        if T.size==1:
            T = np.array([T])
            logfO2 = np.array([logfO2])

        for iT, ilogfO2 in zip(T, logfO2):
            imu_oxides, imu0_oxides = self._calc_liquid_chempot(iT, P, mol_liq)
            iG_gas_species = vapor.eval_gibbs_species(np.array([iT]))

            ilogP, ilnK, ilna = self._calc_gas_species_abundances(
                iT, ilogfO2, imu_oxides, imu0_oxides,
                iG_gas_species, method=method)

            logP.append(ilogP)
            lnK.append(ilnK)
            lna.append(ilna)

        logP = np.array(logP).T
        lnK = np.array(lnK).T
        lna = np.array(lna).T

        species_name = rxn_coefs.index

        logP = pd.DataFrame(data=logP, index=species_name, columns=T)
        
        oxide_melt_comp = pd.Series(self.melt_comp.all_data)
        missing_melt_oxides = oxide_melt_comp[oxide_melt_comp==0].index
        missing_vapor_oxides = [oxide for oxide in missing_melt_oxides 
                                if oxide in rxn_coefs.columns]
        for oxide in missing_vapor_oxides:
            mask = rxn_coefs[oxide]!=0
            logP[mask] = -np.inf

        if full_output:
            return logP, lnK, lna
        else:
            return logP

    def calc_column_density(self, logP, logfO2, g):
        vapor = self.vapor
        species_mass = vapor.species_mass
        species_comps = vapor.species_comps

        Navogadro = 6.022e23
        O2_mass = 2*16

        P_species = 10**logP
        fO2 = 10**logfO2

        density = P_species*Navogadro/(g*species_mass.to_numpy()[:, np.newaxis])
        density = pd.DataFrame(data=density, index=species_mass.index)
        
        return density

    def get_abundant_species(self, density, N=15):
        inds = np.argsort(density.iloc[:,-1].values)[::-1]
        density_high = density.iloc[inds[:N],:]
        return density_high

    def calc_gas_props(self, T, logP):
        R = self.R
        vapor = self.vapor
        species_comps = vapor.get_species_comps()

        Ptotal = np.sum(10**logP, axis=0)
        mol_species_density = 10**logP/(R*T)
        
        result = mol_species_density.values[:,np.newaxis,:]*species_comps.values[:,:,np.newaxis]
        mol_elem_density = pd.DataFrame(data=result.sum(axis=0),
                                        columns=logP.columns, index=species_comps.columns)

        mol_tot = mol_elem_density.sum(axis=0)
        mol_elem_frac = mol_elem_density/mol_tot
        return mol_elem_frac, Ptotal

    def _calc_self_consistent_redox(self, FRAC_STEP=0.9, 
                                    LOG_TOL=1e-4, MAX_ITER=100):
        T = self.T

        for i in range(MAX_ITER):
            logfO2 = self.logfO2 + FRAC_STEP*self.log_oxygen_excess
            self._logfO2 = logfO2
            self._logP_species = self.eval_gas_abundances(T, logfO2)
            
            if np.abs(self.log_oxygen_excess) <= LOG_TOL:
                return
            
        assert False, "self-consistent redox loop did not converge"  

    def _init_vapor(self, vapor_database):
        self._vapor = Vapor(database=vapor_database)

    def _init_liquid(self, liq_mod):
        modelDB = model.Database(liq_mod=liq_mod)
        liq_phs = modelDB.get_phase('Liq')
        OXIDES = np.array(liq_phs.OXIDES)
        
        
        mol_liq_oxides = np.array([
            liq_phs.calc_endmember_comp(imol, method='intrinsic',
                                        output_residual=False)
            for imol in np.eye(OXIDES.size)])[:-1]

        self.modelDB = modelDB
        self.liq_phs = liq_phs
        self.OXIDES = OXIDES
        self.MOL_LIQ_OXIDES = mol_liq_oxides

    def _calc_stoic_log_oxy_excess(self):
        logP = self.logP_species
        P_species = 10**logP
        
        oxygen_present = self._calc_oxygen_present(P_species)
        oxygen_reacted = self._calc_oxygen_reacted(P_species)
        
        log_oxy_excess = np.log10(oxygen_reacted/oxygen_present)
        
        return log_oxy_excess
    
    def _calc_equil_log_oxy_excess(self, T):
        logfO2_equil = self._calc_logfO2_Kress91(T, self.oxide_mols)
        
        dlogfO2 = logfO2_equil - self.logfO2
        return dlogfO2
    
    def _calc_logfO2_Kress91(self, T, oxide_mol_comp):
        P = 1
        return self.modelDB._redox_state_Kress91(
            np.array([T]), np.array([P]), oxide_mol_comp[np.newaxis,:])

    def _calc_oxygen_reacted(self, P_species):
        P_total = P_species.sum()
        P_metal_species = P_species.drop(index=self.vapor.oxygen_species)
        oxygen_reacted_coefs = -self.vapor.get_oxygen_rxn_coefs(self.molar_oxide_comp)
        metal_species_frac = P_metal_species/P_total
        oxygen_reacted = metal_species_frac.T.dot(oxygen_reacted_coefs).iloc[0]
        return oxygen_reacted

    def _calc_oxygen_present(self, P_species):
        P_total = P_species.sum()
        P_oxy_species = P_species.loc[self.vapor.oxygen_species]
        oxy_species_frac = P_oxy_species/P_total
        oxygen_present = oxy_species_frac.T.dot(self.vapor.oxygen_stoic).iloc[0]
        return oxygen_present

    def _calc_liquid_chempot(self, T, P, mol_liq):
        vapor = self.vapor
        # strangely stored in vapor
        melt_comps = vapor.melt_comps

        liq_phs = self.liq_phs
        mol_liq_oxides = self.MOL_LIQ_OXIDES
        R = self.R
        OXIDES = self.OXIDES


        model_oxides = melt_comps.index[1:]
        ind_oxides = [np.where(OXIDES==iox)[0][0] for iox in model_oxides]

        mu0_endmem = np.array([liq_phs.gibbs_energy(T, P, mol=imol)
                               for imol in np.eye(mol_liq.size)])
        mu_endmem = liq_phs.chem_potential(T, P, mol=mol_liq).squeeze()

        mu0_oxides = np.dot(mol_liq_oxides, mu0_endmem)[ind_oxides]
        mu_oxides = np.dot(mol_liq_oxides, mu_endmem)[ind_oxides]

        return mu_oxides, mu0_oxides

    def _calc_gas_species_abundances(self, T, logfO2, mu_oxides, mu0_oxides,
                                     G_gas_species, method='activity'):
        vapor = self.vapor

        R = self.R
        RT = R*T
        rxn_coefs = vapor.rxn_coefs

        LOG10_FAC = np.log10(np.exp(1))
        
        G_O2 = G_gas_species.loc['O2(g)']

        mu_melt_oxides = np.hstack((G_O2, mu_oxides))
        mu0_melt_oxides = np.hstack((G_O2, mu0_oxides))

        ln_fO2 = logfO2/LOG10_FAC
        # 2 different methods here

        if method=='activity':
            dmu_oxides = mu_oxides-mu0_oxides

            ln_a = np.hstack((ln_fO2, dmu_oxides/RT))

            del_G0 = G_gas_species.squeeze()-np.dot(rxn_coefs, mu0_melt_oxides)
            ln_K = +del_G0/RT
            ln_K = pd.Series(data=ln_K, index=rxn_coefs.index)

            ln_P = -(ln_K-np.dot(rxn_coefs, ln_a))
            ln_P = pd.Series(data=ln_P, index=rxn_coefs.index)

            logP = ln_P*LOG10_FAC

        elif method=='chempot':

            dmu_gas_species = +rxn_coefs.dot(mu_melt_oxides)- G_gas_species['T0']
            dmu_RT_O2 = rxn_coefs['O2']*ln_fO2
            ln_P = dmu_gas_species/RT + dmu_RT_O2
            # import code; code.interact(local=locals())
            logP = ln_P*LOG10_FAC
            ln_K = np.nan
            ln_a = np.nan


        else:
            assert False, 'that is not a valid method for calculating gas species abundances. Choose from {activity, chempot}.'


        # from IPython import embed; embed()
        return logP, ln_K, ln_a


    # def fractionate(self, T: float, logfO2: float) -> tuple[pd.DataFrame, pd.DataFrame]:
    #     logP = pd.DataFrame()
    #     sys_comp =pd.DataFrame() 
        
    #     # Initialize
    #     logP0 =  self.eval_gas_abundances(T, logfO2)
    #     logP[0.0] = logP0.iloc[:,0]
    #     sys_comp[0.0] = pd.Series(self.melt_comp.all_data)
        
        
    #     elem_comp_melt = self.melt_comp.elem_comp
    #     elem_comp_melt_dat = pd.Series(elem_comp_melt.data)
        
    #     mol_elem_frac, Ptotal = self.calc_gas_props(T, logP0)
    #     elem_comp_vapor = thermo.ElemMolComp(**mol_elem_frac.iloc[:,0])
    #     elem_comp_vapor_dat = pd.Series(elem_comp_vapor.data)
        
        
    #     periodic_wts = pd.Series(index=thermo.chem.PERIODIC_ORDER,
    #                          data=thermo.chem.PERIODIC_WEIGHTS)
    #     mask = [elem in elem_comp_melt_dat.index for elem in periodic_wts.index]
    #     elem_wts = periodic_wts[mask]
        
    #     total_mass0 = elem_comp_melt_dat.dot(elem_wts)
        
    #     sys_comp[1.0] = pd.Series(self.melt_comp.all_data)
        
    #     return logP, sys_comp
    

class Vapor:
    TREF = None
    R = 8.314
    ATOM_MASS = pd.Series({
        'O' :16,
        'Mg':24.305,
        'Ca':40.078,
        'Al':26.982,
        'Si':28.085,
        'Na':22.99,
        'K' :39.098,
        'Fe':55.845,
        'Ti':47.867,
        'Cr':51.996
    })

    def __init__(self, database):
        self._validate_database(database)
        self._load_vapor_data(database)
        self._init_calc_gibbs_species(database)
        self._init_vapor_data(database)
        self._oxygen_rxn_coefs_FeO = self._calc_oxygen_rxn_coefs({'Fe':1, 'O':1})
        self._oxygen_rxn_coefs_Fe2O3 = self._calc_oxygen_rxn_coefs({'Fe':2, 'O':3})
        

    def _validate_database(self, database):
        assert database in ['LAMOR', 'JANAF0', 'JANAF'], (
            "Chosen database is not a valid choice, choose from ['LAMOR', 'JANAF', 'JANAF0']")
        
    @property
    def species_comps(self):
        return self._species_comps

    @property
    def species_mass(self):
        return self._species_mass

    @property
    def atom_mass(self):
        return self._atom_mass

    @property
    def melt_comps(self):
        return self._melt_comps

    @property
    def rxn_coefs(self):
        return self._rxn_coefs

    @property
    def vapor_dat(self):
        return self._vapor_dat
    
    @property
    def vapor_coefs(self):
        return self._vapor_coefs
    
    @property
    def oxygen_species(self):
        return ['O(g)','O2(g)']
    
    @property
    def oxygen_stoic(self):
        return [1, 2]
    
    def get_oxygen_rxn_coefs(self, oxide_mol_comp):        
        Fe_tot = oxide_mol_comp['FeO'] + 2*oxide_mol_comp['Fe2O3']
        
        frac = oxide_mol_comp['FeO']/Fe_tot
        # frac = 0
        
        oxy_rxn_coefs = (frac*self._oxygen_rxn_coefs_FeO + 
                         (1-frac)*self._oxygen_rxn_coefs_Fe2O3)
        
        return oxy_rxn_coefs
    
    def _calc_oxygen_rxn_coefs(self, FexOy):
        
        oxy_species = self.oxygen_species
        species_comps = self.get_species_comps().drop(index=oxy_species)

        melt_comps = self.melt_comps.drop(index='O2')
        melt_comps.loc['FeO','O'] = FexOy['O']
        melt_comps.loc['FeO','Fe'] = FexOy['Fe']
        
        metals = list(melt_comps.columns.drop('O'))
        O_rxn_coefs = pd.Series(index=species_comps.index, data=0, dtype=float)
        
        for metal in metals:
            melt_component = melt_comps[melt_comps[metal]>0]
            metal_species = species_comps[species_comps[metal]>0]
            
            melt_metal_coef = melt_component[metal].values[0]
            melt_oxy_coef = melt_component['O'].values[0]
            melt_rxn_coef = metal_species[metal]/melt_metal_coef
            metal_O_rxn_coef = metal_species['O']-melt_rxn_coef*melt_oxy_coef
            O_rxn_coefs[metal_O_rxn_coef.index] = metal_O_rxn_coef
        
        return O_rxn_coefs
    
    def _init_calc_gibbs_species(self, database):
        if database=='LAMOR':
            self._calc_gibbs_species = self._calc_gibbs_species_LAMOR
        elif database=='JANAF0':
            self._calc_gibbs_species = self._calc_gibbs_species_JANAF0
            self.TREF = 1900.0
        elif database=='JANAF':
            # self._calc_gibbs_species = self._calc_gibbs_species_JANAF_dummy
            # self._calc_gibbs_species = self._calc_gibbs_species_JANAF
            self._calc_gibbs_species = self._calc_gibbs_species_JANAF_singleT
        
    def _get_vapor_coefs(self, vapor_dat, database):
        if database=='LAMOR':
            cols = ['T_min', 'T_max', 'dG_A', 'dG_B', 'dG_C', 'dG_D', 'dG_E', 'dH298_R']
        elif database in ['JANAF0', 'JANAF']:
            cols = ['T_interval', 'T_min', 'T_max', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
        
        return vapor_dat[cols]
    
    def _get_vapor_species(self, vapor_dat):
        vapor_species = vapor_dat[['state', 'cation', 'cat_num', 'oxy_num']]
        return vapor_species.drop_duplicates()
    
    def _load_vapor_data(self, database):
        vapor_dat = pd.read_csv(self._get_vapor_data_path(database),
                                index_col='species_name')
        vapor_dat = self._clean_vapor_dat_format(vapor_dat)
        vapor_coefs = self._get_vapor_coefs(vapor_dat, database)
        vapor_species = self._get_vapor_species(vapor_dat)
        
        self._vapor_dat = vapor_dat
        self._vapor_coefs = vapor_coefs
        self._vapor_species = vapor_species
    
    def _get_vapor_data_path(self, database):        
        if database=='LAMOR':
            database_file = "vapor-thermo-data.csv"
        elif database=='JANAF':
            database_file = "JANAF-vapor-data-full.csv"
        elif database=='JANAF0':
            database_file = "JANAF0-vapor-data.csv"
            
        with resources.path("vaporock.data", database_file) as f:
            data_file_path = f
        return data_file_path

    def _clean_vapor_dat_format(self, vapor_dat):
        vapor_dat = vapor_dat.fillna(value=0, axis=0)
        vapor_dat.loc['O(g)', 'cation'] = np.nan
        vapor_dat.loc['O2(g)', 'cation'] = np.nan
        return vapor_dat
        

    def _init_vapor_data(self, database):
        vapor_dat = self._vapor_species
        species_comps, species_mass, atom_mass = self._init_species_comp(vapor_dat)
        melt_comps = self._get_melt_oxides_comp(species_comps.columns, database)
        rxn_coefs = self._get_rxn_coefs(species_comps, melt_comps)

        self._species_comps = species_comps
        self._species_mass = species_mass
        self._atom_mass = atom_mass
        self._melt_comps = melt_comps
        self._rxn_coefs = rxn_coefs

    def get_species_comps(self):
        return self._species_comps
    
    def _get_atom_masses_in_database(self, atom_names):
        return self.ATOM_MASS[atom_names]

    def _init_species_comp(self, vapor_dat):
        N_species = vapor_dat.shape[0]
        uniq_cations = vapor_dat['cation'].dropna().unique()
        

        Ncation = uniq_cations.size
        atom_names = np.hstack((['O'],uniq_cations))
        # atom_mass = np.array([16, 24.305, 40.078, 26.982, 28.085, 22.99, 39.098, 55.845])

        atom_mass = self._get_atom_masses_in_database(atom_names)
        
        species_names = vapor_dat.index
        species_comps = np.zeros((N_species, Ncation+1),dtype=int)


        for ind, (cat, cat_num, oxy_num) in enumerate(zip(
            vapor_dat['cation'], vapor_dat['cat_num'], vapor_dat['oxy_num'])):
            ind_cat = np.where(atom_mass.index==cat)
            species_comps[ind, 0] = oxy_num
            species_comps[ind, ind_cat] = cat_num

        species_comps = pd.DataFrame(data=species_comps, columns=atom_mass.index, index=species_names)

        species_mass = species_comps.dot(atom_mass)
        return species_comps, species_mass, atom_mass

    def _get_melt_oxides_comp(self, atom_names, database):
        oxides = ['O2','MgO', 'CaO', 'Al2O3', 'SiO2', 'Na2O', 'K2O', 'FeO', 'TiO2', 'Cr2O3']
        #oxides = ['O2','MgO', 'CaO', 'Al2O3', 'SiO2', 'Na2O', 'K2O', 'FeO']


        Natom = len(atom_names)
        melt_comps = pd.DataFrame(data=0, columns=atom_names, index=oxides)

        melt_comps.loc['O2','O'] = 2

        melt_comps.loc['MgO','Mg'] = 1
        melt_comps.loc['MgO','O'] = 1

        melt_comps.loc['CaO','Ca'] = 1
        melt_comps.loc['CaO','O'] = 1

        melt_comps.loc['Al2O3','Al'] = 2
        melt_comps.loc['Al2O3','O'] = 3

        melt_comps.loc['SiO2','Si'] = 1
        melt_comps.loc['SiO2','O'] = 2

        melt_comps.loc['Na2O','Na'] = 2
        melt_comps.loc['Na2O','O'] = 1

        melt_comps.loc['K2O','K'] = 2
        melt_comps.loc['K2O','O'] = 1

        melt_comps.loc['FeO','Fe'] = 1
        melt_comps.loc['FeO','O'] = 1
        
        # melt_comps.loc['Fe2O3','Fe'] = 2
        # melt_comps.loc['Fe2O3','O'] = 3


        if database in ['JANAF0', 'JANAF']:
        
        	melt_comps.loc['TiO2','Ti'] = 1
        	melt_comps.loc['TiO2','O'] = 2
        
        	melt_comps.loc['Cr2O3','Cr'] = 2
        	melt_comps.loc['Cr2O3','O'] = 3
        # display(melt_comps)

        return melt_comps

    def _get_rxn_coefs(self, species_comps, melt_comps, TOL=1e-6):
        rxn_coefs = pd.DataFrame(data=0, index=species_comps.index,
                                 columns=melt_comps.index, dtype=float)

        for ind, ispecies_name in enumerate(species_comps.T):
            icomp = species_comps.loc[ispecies_name]

            output = np.linalg.lstsq(melt_comps.T,icomp, rcond=None)
            irxn_coefs = output[0]
            irxn_coefs[np.abs(irxn_coefs)<TOL] = 0
            rxn_coefs.loc[ispecies_name] = irxn_coefs

        return rxn_coefs

    ###################

    def eval_gibbs_species(self, T):
        vapor_coefs = self.vapor_coefs
        species_comps = self._species_comps

        G_gas_species = []

        for ispecies_name in species_comps.T:
            icoefs = vapor_coefs.loc[ispecies_name]
            G_gas_species.append(self._calc_gibbs_species(T, icoefs))

        T = np.array([T])
        T_labels = ['T'+str(ind) for ind in range(T.size)]
        
        G_gas_species = pd.DataFrame(
            np.array(G_gas_species), index=species_comps.index,
            columns=T_labels)

        return G_gas_species

    def _calc_gibbs_species_LAMOR(self, T, coefs):
        G_coefs = coefs[['dG_A', 'dG_B', 'dG_C', 'dG_D', 'dG_E']].values
        dH298 = coefs['dH298_R']
        
        R = self.R
        G_scale = np.array([1,1e3,1e6,1e9,1e12])
        dH_scale = 1e3

        G_poly_coefs = (G_coefs/G_scale)[::-1]
        G_species = -R*T*np.polyval(G_poly_coefs, T) + R*dH298*dH_scale
        return G_species
    
    def _calc_gibbs_species_JANAF0(self, T, coefs):
        return self._janaf_G(T, coefs)
    
    def _get_coef_T_bounds(self, coefs, MAXVAL=1e8):
        T_min, T_max = coefs['T_min'], coefs['T_max']
        
        if np.isscalar(T_min):
            T_min = 0
        else:
            #T_min[np.argmin(T_min)] = 0
            T_min = T_min.replace(T_min.min(), 0)
        


        if np.isscalar(T_max):
            T_max = MAXVAL
        else:
            #T_max[np.argmax(T_max)] = MAXVAL
            T_max = T_max.replace(T_max.max(), MAXVAL)
            
        return T_min, T_max
    
    def _calc_gibbs_species_JANAF_dummy(self, T, coefs):
        Tref = 1900.0
        
        T_min, T_max = self._get_coef_T_bounds(coefs)    
        mask = (Tref > T_min)&(Tref <= T_max)
        if type(mask) is pd.Series:
            coefs_ref = coefs.loc[mask].squeeze()
        else:
            coefs_ref = coefs
            
        return self._janaf_G(T, coefs_ref)
    
    def _calc_gibbs_species_JANAF_singleT(self, T, coefs):
        iT  = T[0]
        
        T_min, T_max = self._get_coef_T_bounds(coefs)    
        mask = (iT > T_min)&(iT <= T_max)
        if type(mask) is pd.Series:
            coefs_ref = coefs.loc[mask].squeeze()
        else:
            coefs_ref = coefs
            
        return self._janaf_G(T, coefs_ref)
    
    def _calc_gibbs_species_JANAF(self, T, coefs):


        if type(coefs) is pd.Series:
            return self._janaf_G(T, coefs)
        
        scalarT = False
        
        if np.isscalar(T):
            scalarT = True
            T = np.array([T])
            

            
        T_min, T_max = self._get_coef_T_bounds(coefs)
        # condlist = [T<T_max, T>=T_min]
        condlist = []
        
        funclist = []
        # ind=0
        # for _, icoef in coefs.iterrows():
        for ind in range(coefs.shape[0]):
            icoef = coefs.iloc[ind]

            iTmin, iTmax = T_min.values[ind], T_max.values[ind]

            Trange_mask = (T<iTmax)&(T>=iTmin)
            
            condlist.append(Trange_mask)
            funclist.append(lambda T: self._janaf_G(T, icoef))
            # ind += 1
        
        
        if coefs.index[0] == 'Mg2(g)':
            print(f'condlist = {condlist}')
            print(f'funclist = {funclist}')
            print(f'coefs = {coefs}')
            
        values = np.piecewise(T, condlist, funclist)
        
        if scalarT:
            return values[0]
        else:
            return values
    
    def _janaf_G(self, T, coefs):
        R = self.R
        dH_scale = 1e3
        dH = (self._janaf_dH(T, coefs) ) * dH_scale
        # NOTE: H has been removed bc it is not relevant for some reason... reproduces Lamoreaux now
        #  or unnecessary
        S = self._janaf_S(T, coefs)
        G = dH - T * S
        return G

    def _janaf_dH(self, T, coefs):
        t = T / 1e3
        dH = (coefs['A'] * t + coefs['B'] / 2 * t ** 2 + coefs['C'] / 3 * t ** 3
              + coefs['D'] / 4 * t ** 4 - coefs['E'] / t) + coefs['F']
        # kJ/mol
        return dH

    def _janaf_S(self, T, coefs):
        t = T / 1e3
        S = (coefs['A'] * np.log(t) + coefs['B'] * t + coefs['C'] / 2 * t ** 2
             + coefs['D'] / 3 * t ** 3 - coefs['E'] / 2 / t ** 2) + coefs['G']
        # J/mol/K
        return S
