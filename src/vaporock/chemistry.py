import dataclasses

import numpy as np
import pandas as pd
import thermoengine as thermo
from thermoengine import chem as chemobj
from thermoengine import chemistry as thermochem

OXIDE_MOLWT = {
    'SiO2':   60.0848,
    'TiO2':   79.8988,
    'Al2O3': 101.96128,
    'Fe2O3': 159.6922,
    'Cr2O3': 151.9902,
    'FeO':    71.8464,
    'MnO':    70.9374,
    'MgO':    40.3044,
    'NiO':    74.7094,
    'CoO':    74.9326,
    'CaO':    56.0794,
    'Na2O':   61.97894,
    'K2O':    94.1954,
    'P2O5':  141.94452,
    'H2O':    18.0152,
    'CO2':    44.0098,
}

ELEMS = ['O', 'Si', 'Ti', 'Al', 'Fe', 'Cr', 'Mn', 'Mg',
         'Ni', 'Co', 'Ca', 'Na', 'K', 'P', 'H', 'C']


def _get_oxide_stoichiometry():
    OXIDES = thermo.Oxides()
    oxide_names = [field.name for field in dataclasses.fields(OXIDES)]

    oxide_stoic = {}
    for oxide_name in oxide_names:
        oxide = OXIDES.__getattribute__(oxide_name)
        all_elem_comp = oxide.elem_comp.all_data
        elem_comp = pd.Series({elem:all_elem_comp[elem] for elem in ELEMS},
                            name=oxide_name)
        oxide_stoic[oxide_name] = elem_comp
        
    return pd.DataFrame(oxide_stoic).T

OXIDE_STOIC = _get_oxide_stoichiometry()
MODELDB_DEFAULT = thermo.model.Database()


def redox_buffer(T, buffer, dlogfO2=0, P=1):
    logfO2 = MODELDB_DEFAULT.redox_buffer(
        T, P, buffer=buffer)+dlogfO2
    return logfO2


def _convert_to_oxide_wt_comp(comp_a):
    if type(comp_a) is not thermo.OxideWtComp:
        comp_a = thermo.OxideWtComp(**comp_a)
        
    return comp_a

def convert_comp_to_elem_comp(comp:thermo.OxideWtComp, elems=None):
    return pd.Series(comp.elem_comp.data, index=elems).fillna(0)

def missing_components(comp):
    return set(comp[comp==0].index)

def comp_are_close(comp_a, comp_b):
    comp_a = _convert_to_oxide_wt_comp(comp_a)
    comp_b = _convert_to_oxide_wt_comp(comp_b)
        
    a = pd.Series(comp_a.all_data)
    b = pd.Series(comp_b.all_data)
    return np.allclose(a, b)

def elem_comp_are_close(elem_comp_a:pd.Series, elem_comp_b:pd.Series, TOL=1e-6):
    return np.allclose(elem_comp_a - elem_comp_b, 0, atol=TOL)


def calc_elem_comp_mass(elem_comp:dict):
    oxide_mol_comp = elem_to_oxide_mol_comp(elem_comp)
    oxide_wt_comp = oxide_mol_to_oxide_wt_comp(oxide_mol_comp)
    oxide_wt_comp = pd.Series(oxide_wt_comp.all_data)
    mass = oxide_wt_comp.sum()
    return mass

def elem_to_oxide_mol_comp(elem_comp_input:dict):
    elem_mol_comp = thermochem.ElemMolComp(**elem_comp_input)
    elem_comp = pd.Series(elem_mol_comp.data, index=OXIDE_STOIC.columns).fillna(0)
    
    TOL = 1e-4
    result = np.linalg.lstsq(OXIDE_STOIC.T, elem_comp, rcond=None)
    mol_oxide_comp = pd.Series(result[0], index=OXIDE_STOIC.index)
    mol_oxide_comp[np.abs(mol_oxide_comp) < TOL] = 0
    
    # NOTE error if giving dict instead of **
    oxide_mol_comp = thermochem.OxideMolComp(**mol_oxide_comp)
    return oxide_mol_comp

def oxide_mol_to_oxide_wt_comp(oxide_mol_comp):
    OXIDE_WT = thermochem.OxideWt()
    oxide_wts_ser = pd.Series(dataclasses.asdict(OXIDE_WT))
    oxide_mols_ser = pd.Series(oxide_mol_comp.all_data)
    oxide_wt_comp = oxide_mols_ser*oxide_wts_ser
    return thermochem.OxideWtComp(**oxide_wt_comp)

def format_oxide_wt_comp(melt_comp_wts:dict) -> np.array:
    return chemobj.format_mol_oxide_comp(melt_comp_wts)

def wt_to_mol_oxide(oxide_wts:np.array) -> np.array:
    return oxide_wts/pd.Series(OXIDE_MOLWT).values
    # return chemobj.format_mol_oxide_comp(melt_comp_wts, convert_grams_to_moles=True)
    # return chemobj.wt_to_mol_oxide(oxide_wts)

def calc_liq_endmem_comp(oxide_mols:np.array, liq_phs) -> np.array:
    return liq_phs.calc_endmember_comp(
        oxide_mols, method='intrinsic', output_residual=False)

def reset_Fe_redox(init_comp:dict, Fe2O3_FeO_ratio:float) -> dict:
    assert Fe2O3_FeO_ratio > 0, (
        'Fe2O3_FeO_ratio is a ratio is a molar ratio, so it must be strictly positive.')
    init_comp = _standardize_Fe_oxide_content(init_comp)
   
    
    mol_Fe_tot = _calc_total_Fe(init_comp)
    mol_FeO, mol_Fe2O3 = _calc_moles_from_Fe2O3_FeO_ratio(Fe2O3_FeO_ratio, mol_Fe_tot)
    
    comp = init_comp.copy()
    comp['FeO'] = mol_FeO*OXIDE_MOLWT['FeO']
    comp['Fe2O3'] = mol_Fe2O3*OXIDE_MOLWT['Fe2O3']
    return comp

def _calc_moles_from_Fe2O3_FeO_ratio(Fe2O3_FeO_ratio, mol_Fe_tot):
    mol_FeO = mol_Fe_tot/(2*Fe2O3_FeO_ratio+1)
    mol_Fe2O3 = 0.5*(mol_Fe_tot - mol_FeO)
    return mol_FeO, mol_Fe2O3

def _calc_total_Fe(init_comp):
    mol_FeO_init = init_comp['FeO']/OXIDE_MOLWT['FeO']
    mol_Fe2O3_init = init_comp['Fe2O3']/OXIDE_MOLWT['Fe2O3']
    mol_Fe_tot = 2*mol_Fe2O3_init + 1*mol_FeO_init
    return mol_Fe_tot

def _standardize_Fe_oxide_content(init_comp):
    if 'Fe2O3' not in init_comp:
        init_comp['Fe2O3'] = 0
    if 'FeO' not in init_comp:
        init_comp['FeO'] = 0
        
    return init_comp

