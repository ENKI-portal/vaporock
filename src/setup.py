import pathlib

from setuptools import find_namespace_packages, find_packages, setup

HERE = pathlib.Path(__file__).parent

VERSION = '0.1.0'
PACKAGE_NAME = 'vaporock'
AUTHOR = 'Aaron S. Wolf'
AUTHOR_EMAIL = 'aswolf@umich.edu'
URL = 'https://gitlab.com/ENKI-portal/vaporock.git'

LICENSE ='GNU AFFERO GENERAL PUBLIC LICENSE Version 3'
DESCRIPTION = 'Thermodynamics of vaporized silicate rocks & melts for modeling magma ocean atmospheres and stellar nebula'
LONG_DESCRIPTION = (HERE / "README.md").read_text()
LONG_DESC_TYPE = "text/markdown"

INSTALL_REQUIRES = [
      'numpy',
      'pandas',
      'thermoengine',
      'pytest',      
]

setup(name=PACKAGE_NAME,
      version=VERSION,
      description=DESCRIPTION,
      long_description=LONG_DESCRIPTION,
      long_description_content_type=LONG_DESC_TYPE,
      author=AUTHOR,
      license=LICENSE,
      author_email=AUTHOR_EMAIL,
      url=URL,
      install_requires=INSTALL_REQUIRES,
      packages=find_packages(),
      package_dir={'vaporock': 'vaporock'},
      package_data={'vaporock': ['data/*.csv']},
      # packages=find_namespace_packages(where="vaporock"),
      # package_dir={"": "vaporock"},
      # package_data={
      #     "data": ["*.csv"],
      # }
)

