.PHONY: tests nbtests devinstall pyinstall

tests:

ifdef args
	pytest -c tests/pytest.ini $(args)
else
	pytest -c tests/pytest.ini
endif

nbtests:
	pytest --nbval notebooks/_run_all_notebooks_.ipynb

pyinstall:
	cd src; pip install --upgrade .
    
devinstall:
	cd src; pip install --upgrade -e .
