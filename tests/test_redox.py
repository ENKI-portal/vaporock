import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import thermoengine as thermo
from cycler import cycler
from IPython import display
from pytest import mark

import vaporock
from vaporock import chemistry as chem

allclose = np.allclose

def should_equilibrate_with_O2_buffer(LOG_TOL=0.01):
    # Remove this unnecessary test?
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    system = vaporock.System(comp=BSE_comp, vapor_database='JANAF')
    system.equilibrate(T=2000, O2_buffer='IW')

    assert np.abs(system.log_oxygen_excess) > LOG_TOL

def should_equilibrate_using_self_consistent_redox(LOG_TOL=0.01):
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'Fe2O3': .3, 'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    system = vaporock.System(comp=BSE_comp, vapor_database='JANAF')
    system.equilibrate(T=2000, intrinsic_O2=True, O2_method='equil')

    assert np.abs(system.log_oxygen_excess) < LOG_TOL
    
def should_equilibrate_using_stoichiometric_self_consistent_redox(LOG_TOL=0.01):
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'Fe2O3': .3, 'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    system = vaporock.System(comp=BSE_comp, vapor_database='JANAF')
    system.equilibrate(T=2000, intrinsic_O2=True, O2_method='stoic')

    assert np.abs(system.log_oxygen_excess) < LOG_TOL

def should_yield_fO2_sensitive_to_melt_redox(dlogfO2_min=2.0):
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'Fe2O3': 0.3, 'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 
                'TiO2': 0.18}
    
    Fe2O3_FeO_ratio = 10
    oxidized_BSE_comp = chem.reset_Fe_redox(BSE_comp, Fe2O3_FeO_ratio)
    
    system = vaporock.System(comp=BSE_comp, vapor_database='JANAF')
    system.equilibrate(T=2000, intrinsic_O2=True, O2_method='equil')
    
    system_oxidized = vaporock.System(comp=oxidized_BSE_comp, vapor_database='JANAF')
    system_oxidized.equilibrate(T=2000, intrinsic_O2=True, O2_method='equil')
    
    assert system_oxidized.logfO2 > system.logfO2
    assert system_oxidized.logfO2 > system.logfO2 + dlogfO2_min
    
def should_show_stoichiometric_redox_insensitive_to_oxidation_state(LOG_TOL=0.2):
    # Maybe remove from tests... move to vaporedox project?
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24, 
                'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    # oxidized_BSE_comp = chem.reset_Fe_redox(BSE_comp, Fe2O3_frac=0.5)
    Fe2O3_FeO_ratio = 10
    oxidized_BSE_comp = chem.reset_Fe_redox(BSE_comp, Fe2O3_FeO_ratio)

    
    system = vaporock.System(comp=BSE_comp, vapor_database='JANAF')
    system.equilibrate(T=2000, intrinsic_O2=True, O2_method='stoic')
    
    system_oxidized = vaporock.System(comp=oxidized_BSE_comp, vapor_database='JANAF')
    system_oxidized.equilibrate(T=2000, intrinsic_O2=True, O2_method='stoic')
    
    assert np.abs(system_oxidized.logfO2 - system.logfO2) < LOG_TOL

def should_recover_fO2_from_predicted_O2_abundance():
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'Fe2O3': 0.3, 'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    T = 2000
    system = vaporock.System(comp=BSE_comp)
    system.equilibrate(T=T, intrinsic_O2=True)

    assert allclose(system.logfO2, system.logP_species.loc['O2(g)'])
    
            