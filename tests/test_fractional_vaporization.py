import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import thermoengine as thermo
from cycler import cycler
from IPython import display
from pytest import mark

import vaporock
from vaporock import chemistry as chem

allclose = np.allclose


def should_extract_mass_from_system(frac_removed=0.05, TOL=1e-5):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'Fe2O3': .3, 'CaO': 3.78}

    system = vaporock.System(comp=comp)
    mass_init = system.mass
    system.extract_vapor(T=2000, frac_removed=frac_removed)
    
    frac_remaining = system.mass/mass_init
    assert allclose(frac_remaining + frac_removed, 1, atol=TOL)
    
def should_extract_mass_from_system_using_O2_buffer(frac_removed=0.05, TOL=1e-5):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'Fe2O3': .3, 'CaO': 3.78}

    system = vaporock.System(comp=comp)
    mass_init = system.mass
    system.extract_vapor(T=2000, frac_removed=frac_removed, O2_buffer='QFM')
    
    frac_remaining = system.mass/mass_init
    assert allclose(frac_remaining + frac_removed, 1, atol=TOL)
    
# Try with and w/o Na, K, Ti
def should_conserve_elemental_comp_during_extraction(frac_removed=.03, TOL=1e-4):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'Fe2O3': .3, 'CaO': 3.78}

    
    system = vaporock.System(comp=comp)
    elem_comp_init = system.melt_elem_comp
    vapor_removed = system.extract_vapor(T=2000, frac_removed=frac_removed)
    
    elem_comp_final = system.melt_elem_comp
    
    assert chem.elem_comp_are_close(vapor_removed+elem_comp_final, 
                                    elem_comp_init, TOL=TOL)
    return None

def should_reach_target_mass_frac_when_buffered(frac_retained=0.1, TOL=1e-5):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'CaO': 3.78}
    system = vaporock.System(comp=comp)
    mass_init = system.mass
    system.fractionate(T=2000, O2_buffer='QFM', melt_frac_retained=frac_retained)
    
    frac_remaining = system.mass/mass_init
    assert frac_remaining <= frac_retained
    
def should_reach_target_mass_frac(frac_retained=0.1):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24, 'Fe2O3':0.3,
            'CaO': 3.78}
    system = vaporock.System(comp=comp)
    mass_init = system.mass
    system.fractionate(T=2000, melt_frac_retained=frac_retained, frac_removed=0.01)
    
    frac_remaining = system.mass/mass_init
    assert frac_remaining <= frac_retained
    
@mark.xfail
def should_conserve_elemental_comp_across_all_vapor_extracted(frac_retained=0.9, 
                                                            TOL=1e-4):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'Fe2O3': .3, 'CaO': 3.78}

    
    system = vaporock.System(comp=comp)
    elem_comp_init = system.melt_elem_comp
    vapor_extracted = system.fractionate(T=2000, melt_frac_retained=frac_retained, frac_removed=0.01)
    
    elem_comp_extracted = vapor_extracted.sum(axis=0) 
    elem_comp_final = system.melt_elem_comp
    
    assert allclose(elem_comp_final + elem_comp_extracted, elem_comp_init)
        
@mark.xfail
def should_avoid_over_extracting_any_elements(TOL=1e-4):
    comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
            'Fe2O3': .3, 'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}

    system = vaporock.System(comp=comp)
    mass_init = system.mass
    system.extract_vapor(T=2000, frac_removed=0.99)
    
    assert np.all(system.melt_elem_comp > 0)
   


