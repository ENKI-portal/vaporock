import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import thermoengine as thermo
from cycler import cycler
from IPython import display
from pytest import mark

import vaporock
from vaporock import chemistry as chem

allclose = np.allclose

def should_predict_species_abundances():
    T = np.linspace(1500, 3000, 5)
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}
    species = ['O(g)', 'O2(g)', 'Mg(g)', 'MgO(g)', 'Mg2(g)', 'Ca(g)', 'CaO(g)', 'Ca2(g)',
                'Al(g)', 'AlO(g)', 'AlO2(g)', 'Al2(g)', 'Al2O(g)', 'Al2O2(g)', 'Si(g)',
                'SiO(g)', 'SiO2(g)', 'Si2(g)', 'Si2O2(g)', 'Si3(g)', 'Na(g)', 'NaO(g)',
                'Na2(g)', 'Na2O(g)', 'K(g)', 'KO(g)', 'KO2(g)', 'K2(g)', 'K2O(g)',
                'Fe(g)', 'FeO(g)']

    logP_expected = np.array(
        [[-10.44654292, -6.85401763, -4.47040881, -2.77626437, -1.5126835],
            [-10.10196401, -6.46259899, -4.0660656, -2.37543329, -1.12332221],
            [-10.10974438, -6.86471667, -4.78458609, -3.35729362, -2.32718402],
            [-13.82966854, -9.28791401, -6.34456543, -4.30706532, -2.828041],
            [-23.25404527, -16.91587338, -12.89310894, -10.1562411, -8.16561576],
            [-16.32142048, -12.16477793, -9.46828964, -7.59502539, -6.2275292],
            [-18.74336383, -13.70036705, -10.41964443, -8.1252949, -6.42733759],
            [-35.35579173, -27.20088768, -21.95618787, -18.33875431, -15.67943435],
            [-18.67920262, -13.61955902, -10.2878442, -7.93619211, -6.19324476],
            [-17.27172542, -12.22139579, -8.90452282, -6.56785316, -4.83405177],
            [-20.27859178, -14.42459933, -10.61933896, -7.95841849, -5.92436363],
            [-36.48493136, -27.51577213, -21.61223938, -17.4469068, -14.36129892],
            [-23.77042497, -17.41249836, -13.30200219, -10.44385182, -8.27726425],
            [-23.8511406, -17.37804231, -13.21400162, -10.33059158, -8.12599622],
            [-20.75783823, -15.13818844, -11.40111852, -8.73619915, -6.740253],
            [-9.93794568, -6.35830243, -4.00171478, -2.33848754, -1.10570734],
            [-11.55066532, -7.65833781, -5.09900474, -3.29564614, -1.96141476],
            [-35.48124312, -26.58558052, -20.67244207, -16.45938974, -13.30620626],
            [-19.87121708, -13.76332881, -9.7370239, -6.89118169, -4.77875809],
            [-49.07402946, -37.35118786, -29.55311983, -23.98594005, -19.81049259],
            [-6.09179121, -4.08407273, -2.77416061, -1.85655231, -1.17552014],
            [-12.4808337, -8.66520563, -6.16568188, -4.41230884, -3.11670464],
            [-13.85071226, -10.35999437, -8.09839813, -6.53353509, -5.39703701],
            [-16.38492074, -12.23934115, -9.54258667, -7.66374947, -6.28216209],
            [-7.37292951, -5.27897369, -3.91180489, -2.95591402, -2.25235589],
            [-12.61089257, -8.8999338, -6.47356206, -4.77592581, -3.52617453],
            [-19.77650788, -14.62406276, -11.2502588, -8.88457266, -7.13722131],
            [-16.79484228, -13.01475155, -10.57350287, -8.88842445, -7.66449366],
            [-18.84544576, -14.50027664, -11.67920335, -9.71929308, -8.28585723],
            [-8.48453496, -5.82450185, -4.10157697, -2.90735997, -2.03874535],
            [-10.20865326, -6.91470849, -4.7899748, -3.32436767, -2.2645425]]
    )

    system = vaporock.System(vapor_database='LAMOR')
    system.set_melt_comp(melt_comp_wts)

    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP, lnK, lna = system.eval_gas_abundances(T, logfO2, full_output=True, method='activity')
    
    
    assert np.all(species==logP.index)
    assert np.allclose(logP, logP_expected)
    
def should_predict_vapor_densities():
    T = np.array([1500, 2500])
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5
    g = 36.2

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}
    species = ['O(g)', 'O2(g)', 'Mg(g)', 'MgO(g)', 'Mg2(g)', 'Ca(g)', 'CaO(g)', 'Ca2(g)',
                'Al(g)', 'AlO(g)', 'AlO2(g)', 'Al2(g)', 'Al2O(g)', 'Al2O2(g)', 'Si(g)',
                'SiO(g)', 'SiO2(g)', 'Si2(g)', 'Si2O2(g)', 'Si3(g)', 'Na(g)', 'NaO(g)',
                'Na2(g)', 'Na2O(g)', 'K(g)', 'KO(g)', 'KO2(g)', 'K2(g)', 'K2O(g)',
                'Fe(g)', 'FeO(g)']

    density_expected = np.array([
        [3.71851280e+10, 5.41165838e+17],
        [4.11072282e+10, 6.84718826e+17],
        [5.31608814e+10, 1.13733824e+17],
        [6.10948659e+06, 5.06319190e+15],
        [1.90660745e-03, 3.71106855e+09],
        [1.98018451e+04, 2.92837679e+12],
        [5.35643491e+01, 4.60211409e+11],
        [9.14754206e-16, 8.02893350e+00],
        [1.29049233e+02, 1.41640025e+12],
        [2.07023735e+03, 2.09896742e+13],
        [1.48498102e+00, 4.96326235e+11],
        [1.00924519e-16, 6.27724365e+01],
        [4.03396923e-04, 1.19995239e+09],
        [2.72630828e-04, 1.24572403e+09],
        [1.03447386e+00, 1.72675321e+11],
        [4.35307007e+10, 5.51246799e+17],
        [7.79113622e+08, 4.05695809e+16],
        [9.77877909e-16, 5.61316586e+02],
        [2.53801544e+00, 3.41852141e+12],
        [1.66497238e-29, 4.35708218e-06],
        [5.85736113e+14, 5.36055391e+18],
        [1.41008486e+08, 4.95414780e+15],
        [5.10212330e+06, 3.63846347e+13],
        [1.10626677e+04, 1.60711359e+12],
        [1.80280242e+13, 2.44510639e+17],
        [7.39611711e+07, 1.57891856e+15],
        [3.91441548e+00, 6.01642527e+10],
        [3.41197122e+03, 8.70982056e+10],
        [2.52089038e+01, 8.81892958e+09],
        [9.76141823e+11, 1.63039891e+17],
        [1.43212978e+10, 4.03813710e+16]])

    system = vaporock.System(vapor_database='LAMOR')
    system.set_melt_comp(melt_comp_wts)

    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP, lnK, lna = system.eval_gas_abundances(T, logfO2, full_output=True, method='activity')
    
    density = system.calc_column_density(logP, logfO2, g)
    
    assert np.all(species==density.index)
    assert np.allclose(density, density_expected)
    
def should_support_distinct_vapor_databases():
    system_LAMOR = vaporock.System(vapor_database='LAMOR')
    coefs_LAMOR = system_LAMOR.vapor.vapor_coefs

    system_JANAF0 = vaporock.System(vapor_database='JANAF0')
    coefs_JANAF0 = system_JANAF0.vapor.vapor_coefs
    
    assert not coefs_LAMOR.equals(coefs_JANAF0)
    
def should_speciate_using_JANAF0():
    # Maybe remove this one... 
    # Important for development but not ongoing testing...
    T = 1900.0
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}
    species = [
        'O(g)', 'O2(g)', 'Mg(g)', 'MgO(g)', 'Mg2(g)', 'Ca(g)', 'CaO(g)',
        'Ca2(g)', 'Al(g)', 'AlO(g)', 'AlO2(g)', 'Al2(g)', 'Al2O(g)', 'Al2O2(g)',
        'Si(g)', 'SiO(g)', 'SiO2(g)', 'Si2(g)', 'Si3(g)', 'Na(g)', 'NaO(g)',
        'Na2(g)', 'K(g)', 'KO(g)', 'K2(g)', 'Fe(g)', 'FeO(g)', 'TiO(g)',
        'Ti(g)', 'TiO2(g)', 'Cr(g)', 'CrO(g)', 'CrO2(g)', 'CrO3(g)']

    logP_expected = np.array([
        [ -6.66171869],
        [ -6.27238215],
        [ -6.68834046],
        [ -9.045846  ],
        [-17.20942053],
        [-11.93962132],
        [-13.44932581],
        [-26.96341247],
        [-13.35796279],
        [-11.94478619],
        [-14.17141743],
        [-26.99390695],
        [-17.12795874],
        [-17.11007371],
        [-14.81407362],
        [ -6.12313313],
        [ -7.89783383],
        [-26.82179119],
        [-37.38427221],
        [ -3.98252512],
        [ -7.88020325],
        [-10.16713543],
        [ -5.17198253],
        [ -9.07280677],
        [-12.74173409],
        [ -5.6818631 ],
        [ -6.74151414],
        [-np.inf],
        [-np.inf],
        [-np.inf],
        [-np.inf],
        [-np.inf],
        [-np.inf],
        [-np.inf]])

    system = vaporock.System(vapor_database='JANAF0')
    system.set_melt_comp(melt_comp_wts)

    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP, lnK, lna = system.eval_gas_abundances(T, logfO2, full_output=True, method='activity')
    print(f'logP = {repr(logP.values)}')
    
    assert np.all(species==logP.index)
    assert np.allclose(logP, logP_expected)
    
def should_have_distinct_vapor_coefs_for_JANAF0():
    # Delete this test, revealse internal details...
    system_JANAF0 = vaporock.System(vapor_database='JANAF0')
    coefs_JANAF0 = system_JANAF0.vapor.vapor_coefs
    
    system_JANAF = vaporock.System(vapor_database='JANAF')
    coefs_JANAF = system_JANAF.vapor.vapor_coefs
    
    assert not coefs_JANAF.equals(coefs_JANAF0)
    
def should_be_equiv_using_JANAF0_and_JANAF_at_Tref():
    # Maybe delete?
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}
    
    system_JANAF0 = vaporock.System(vapor_database='JANAF0')        
    system_JANAF = vaporock.System(vapor_database='JANAF')
    
    system_JANAF0.set_melt_comp(melt_comp_wts)
    system_JANAF.set_melt_comp(melt_comp_wts)
    
    TREF = system_JANAF0.vapor.TREF
    T = np.array([TREF-1, TREF])
    
    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP_JANAF0, _, _ = system_JANAF0.eval_gas_abundances(
        T, logfO2, full_output=True, method='activity')
    
    logP_JANAF, _, _ = system_JANAF.eval_gas_abundances(
        T, logfO2, full_output=True, method='activity')
    
    
    print(f'JANAF: {logP_JANAF}')
    print(f'JANAF0: {logP_JANAF0}')
    print(logP_JANAF-logP_JANAF0)
    icoefs_JANAF = system_JANAF.vapor.vapor_coefs.loc["Mg2(g)"]
    icoefs_JANAF0 = system_JANAF0.vapor.vapor_coefs.loc["Mg2(g)"]
    del_coefs = icoefs_JANAF-pd.concat([icoefs_JANAF0.to_frame()]*4, axis=1).T
    print(f'del_coefs = {del_coefs.iloc[1]}')
    print(f'{icoefs_JANAF.iloc[1]}')
    assert np.allclose(logP_JANAF, logP_JANAF0)
    
def should_have_JANAF0_deviate_from_JANAF_at_high_T():
    T = 5000.0
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}
    
    system_JANAF0 = vaporock.System(vapor_database='JANAF0')        
    system_JANAF = vaporock.System(vapor_database='JANAF')
    
    system_JANAF0.set_melt_comp(melt_comp_wts)
    system_JANAF.set_melt_comp(melt_comp_wts)

    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP_JANAF0, _, _ = system_JANAF0.eval_gas_abundances(
        T, logfO2, full_output=True, method='activity')
    
    logP_JANAF, _, _ = system_JANAF.eval_gas_abundances(
        T, logfO2, full_output=True, method='activity')
    
    print(logP_JANAF-logP_JANAF0)
    assert not np.allclose(logP_JANAF, logP_JANAF0)
    
def should_give_equiv_results_using_activity_and_chempot_methods():
    T = 1900.0
    P = 1e-10
    buffer = 'IW'
    dlogfO2 = +1.5

    # BSE composition from Schaefer & Fegley (TiO2 neglected)
    melt_comp_wts = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                        'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04}

    system_JANAF = vaporock.System(vapor_database='JANAF')
    system_JANAF.set_melt_comp(melt_comp_wts)
    
    
    
    logfO2 = vaporock.redox_buffer(T, buffer=buffer, P=P, dlogfO2=dlogfO2)
    logP_activity, _, _ = system_JANAF.eval_gas_abundances(
        T, logfO2, full_output=True, method='activity')
    
    logP_chempot, _, _ = system_JANAF.eval_gas_abundances(
        T, logfO2, full_output=True, method='chempot')
    
    assert np.allclose(logP_activity, logP_chempot)
