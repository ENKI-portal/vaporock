import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import thermoengine as thermo
from cycler import cycler
from IPython import display
from pytest import mark

import vaporock
from vaporock import chemistry as chem

allclose = np.allclose


def should_store_melt_comp():
    system = vaporock.System(vapor_database='JANAF')
    melt_comp_wts = {'SiO2':45.97, 'MgO':36.66, 'Al2O3':4.77, 'FeO':8.24, 
                        'CaO':3.78, 'Na2O':0.35, 'K2O':0.04}
    system.set_melt_comp(melt_comp_wts)
    
    assert system.melt_comp == melt_comp_wts

@mark.xfail   # Does not accept dict input currently, so method made private
def should_adjust_melt_comp():
    mol_MgSiO3 = {'MgO': chem.OXIDE_MOLWT['MgO'], 'SiO2':chem.OXIDE_MOLWT['SiO2']}
    mol_MgO = {'MgO':chem.OXIDE_MOLWT['MgO']}
    
    system = vaporock.System(comp=mol_MgSiO3)
    system._adjust_comp({'Si':-1, 'O':-2})
    
    assert chem.comp_are_close(system.melt_comp, mol_MgO)

def should_retrieve_system_mass():
    comp = {'SiO2':50, 'MgO': 50}
    
    system = vaporock.System(comp=comp)
    assert system.mass == 100
    
def should_store_total_pressure():
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    system = vaporock.System(comp=BSE_comp)
    system.equilibrate(T=2000, O2_buffer='IW')
    P_total_expected = 0.0007135441628164863
    
    assert allclose(system.P, P_total_expected)

def should_recover_fO2_from_O2_buffer():
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24,
                'CaO': 3.78, 'Na2O': 0.35, 'K2O': 0.04, 'TiO2': 0.18}
    
    T = 2000
    O2_buffer = 'IW'
    system = vaporock.System(comp=BSE_comp)
    system.equilibrate(T=T, O2_buffer=O2_buffer)
    
    logfO2_expected = vaporock.redox_buffer(T, O2_buffer)
    
    assert allclose(system.logfO2, logfO2_expected)
    assert allclose(system.logP_species.loc['O2(g)'], 
                    logfO2_expected)
    
def should_get_absent_vapor_species_for_missing_melt_components():
    BSE_comp = {'SiO2': 45.97, 'MgO': 36.66, 'Al2O3': 4.77, 'FeO': 8.24, 'CaO': 3.78}
    
    system = vaporock.System(comp=BSE_comp)
    system.equilibrate(T=2000, O2_buffer='QFM')
    P_species = (10**system.logP_species).iloc[:,0]

    assert P_species['TiO(g)']==0
    assert (chem.missing_components(system.melt_elem_comp) == 
            chem.missing_components(system.vapor_elem_comp))
