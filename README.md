# VapoRock

Thermodynamics of vaporized silicates for modeling volcanic outgassing
& magma ocean atmospheres

# Usage

VapoRock is open source scientific software distributed under the GNU AFFERO GPLv3 License. **If you use this software as a part of any published work, please cite the method paper and VapoRock software repository**:

* Wolf, A.S., Jäggi, N., Sossi, P.A. and Bower, D.J., 2022. VapoRock: Thermodynamics of vaporized silicate melts for modeling volcanic outgassing and magma ocean atmospheres. arXiv preprint arXiv:2208.09582. https://doi.org/10.48550/arXiv.2208.09582

* Wolf, Aaron S., Jäggi, Noah, Sossi, Paolo A., Bower, Dan J., & Ghiorso, Mark S. (2021). VapoRock: Thermodynamics of vaporized silicate rocks & melts for modeling magma ocean atmospheres and stellar nebula (v0.1). Zenodo. https://doi.org/10.5281/zenodo.4594226

If you have any questions or issues, please submit an issue on the issues page in this repository (https://gitlab.com/ENKI-portal/vaporock/-/issues). We are also happy to collaborate with any users who wish to help develop additional functionality using merge requests. 

Thanks for your interest!

# Getting Started
VapoRock is primarily meant to be used through the Jupyter Notebook interface. Many example calculations are provided in the **/notebooks** directory. 

The easiest place to start is the basic demo provided in **/notebooks/VapoRock-basic-demo.ipynb**.

# Installation
## Local Installiation
To install VapoRock, first clone and install the [ThermoEngine](https://gitlab.com/ENKI-portal/ThermoEngine) package, following installation instructions there.

VapoRock requires no additional packages beyond those needed for ThermoEngine, leaving only one remaining step.
Clone the VapoRock repo and issue the following command:
```
make devinstall
```
VapoRock is now installed and can be run on your local computing environment.

## Cloud Computation
Running VapoRock in the cloud is a feature that is coming soon.
This will avoid all complex steps surrounding installation or managing Python environments.
Stay tuned to upcoming developments.

# Contributing
Thanks so much for your interest in further expanding and developing the capabilities of VapoRock! We encourage users to contribute using the collaborative tools available through GitLab. 

Please open an issue to discuss potential future feature requests or feature development. Doing this in advance will ensure that your coding efforts are well spent, and that the code conforms with our style and testing standards. Additional functionality can be merged back into the main repo through merge requests.

## Clean Notebook contents
To remove all output from notebooks for clean commit, run the following command:
`jupyter nbconvert --clear-output --inplace notebooks/*.ipynb`